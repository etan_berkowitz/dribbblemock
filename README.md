# Dribbble Mock -- Etan Berkowitz

Reference: https://dribbble.com/shots/1131390-Joining-Articulate/attachments/144972

##### Tech Choices

- HTML5 BoilerPlate 5.2.0
- BootStrap 3.3.1
- Custom BootStrap CSS BootSwatch (based on Google's Material Design)
- jQuery 1.11.2

#### Author Notes

I wanted to keep this as clean looking as possible. I'm a bit tired of the standard look of Bootstrap 3 and really like Google's Material Design so I chose a hybrid of the two (with a few of my own customizations). 

I also wanted to make sure the page was really interactive. So if you click "Mark Completed" the progress bar will change and add the symbol on the side nav. If you add in some comments not only will the comments appear, but the side nav will extend dynamically. Lastly, if you hover over the comments, the design implied that a Flag / Like dropdown would appear, so I added that as well.

#### To Do

I'd love to make this into more of a live site! Would love to have a light login system, a backend to store comments and remember which items a user has clicked on. Obviously would love to build out the remainder of the pages (browse courses, my profile, etc.) and make it more mobile friendly.

I'd also like to clean up the CSS and use something like SASS or LESS. If the site became more robust, would also like to use a real front-end framework like React.

#### Tested on Macbook Retina 17inch and Macbook Air 13inch on the latest Chrome Browser.