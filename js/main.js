(function(){
  // Comment template for input into form
  function commentTemplate(comment) {
    return "<div class='row main-comment'>" +
    "<div class='col-md-1'>" +
    "<img src='./img/user_icon.jpg' />" +
    "</div>" +
    "<div class='col-md-11'>" +
    "<h5>Jack Dorsey</h5>" +
    "<p>" + comment + "</p>" +
    "<div class='comment-interaction'>" +
    "<div class='comment-like'>" +
    "<span class='glyphicon glyphicon-heart' aria-hidden='true'></span>" +
    "<p>Like</p>" + 
    "<p>0</p>" +
    "</div>" +
    "<div class='comment-flag'>" +
    "<span class='glyphicon glyphicon-flag' aria-hidden='true'></span>" +
    "<p>Flag</p>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</div>"
  };

  // Toggles if the input button for the comment is disabled
  function textareaChecker() {
    setTimeout( function() {
      if ($(".comment-body textarea").val() != "") {
        $(".comment-body button").removeClass("disabled")
      } else {
        $(".comment-body button").addClass("disabled")
      }
    }, 0); 
    // setTimeout is used to make sure the if statement is fired once the stack is cleared
  };

  // Checks height of created comment section and dynamically adds that padding to left nav
  function navExtender() {
    var commentHeight = $(".current-comments").height();

    $(".side-nav-body .lesson-plan").last().css("padding-bottom", commentHeight)
  };

// Using event bubbling since so these event listeners can be used by the preloaded comments and the dynamically loaded ones as well

  // Background color and comment interaction buttons appear
  $(".current-comments, .past-comments").on("mouseenter", ".main-comment .col-md-11, .sub-comment .col-md-9", function() {
    $(this).addClass("comment-active")
    $(this).find(".comment-interaction").fadeIn()
  });

  // Background color and comment interaction buttons disappear
  $(".current-comments, .past-comments").on("mouseleave", ".main-comment .col-md-11, .sub-comment .col-md-9", function() {
    $(this).removeClass("comment-active")
    $(this).find(".comment-interaction").hide()
  });

  // Toggles background color and like count for comments
  $(".comments").on("click", ".comment-like", function(){
    var likeElement = $(this).children().last(),
        likeCount = parseInt(likeElement.text())

    $(this).children().toggleClass("liked-comment")

    if (likeElement.hasClass("liked-comment")) {
      likeElement.text(likeCount + 1)
    } else {
      likeElement.text(likeCount - 1)
    };
  });

  // Toggles background color for flags
  $(".comments").on("click", ".comment-flag", function(){
    $(this).children().toggleClass("flagged-comment")  
  });

  // Action once user clicks comment submit button
  $(".comment-body button").click( function(){
    var commentBody = $(".comment-body textarea").val()

    if (commentBody != "") {
      $(".current-comments").prepend(commentTemplate(commentBody))
      $(".comment-body textarea").val("")
      $(".comment-top .comment-number p").text(parseInt($(".comment-top .comment-number p").text()) + 1)
      textareaChecker()
      navExtender()
    }
  });

  // Checker for enabling submit button
  $(".comment-body textarea").keydown( function() {
    textareaChecker()
  });

  // Add 'live' video to video player
  $(".lesson-container .video-section img").click( function() {
    $(".video-section .col-md-10 img:first-child").toggle()
    $(".video-section .col-md-10 img:last-child").toggle()
  });

  // Class toggle for lesson favorite
  $(".top-mini-panel .sub-header p:nth-child(3)").click( function() {
    $(this).toggleClass("favorite")
  });

  // Class toggle for lesson reuse
  $(".top-mini-panel .sub-header p:nth-child(5)").click( function() {
    $(this).toggleClass("reuse")
  });

  // Class toggle for lesson complete and changes progress bar and marks class complete in timeline
  $(".top-mini-panel .sub-header p:nth-child(7)").click( function() {
    $(this).toggleClass("mark-complete")
    $(".lesson-plan .current").toggleClass("current-complete")

    if($(this).hasClass("mark-complete")) {
      $(".progress-bar").css("width", "26%")
      $(".side-nav-progress .col-md-2 p").text("26%")
    } else {
      $(".progress-bar").css("width", "13%")
      $(".side-nav-progress .col-md-2 p").text("13%")
    }
  });
})();